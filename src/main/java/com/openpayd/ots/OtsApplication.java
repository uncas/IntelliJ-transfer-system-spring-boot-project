package com.openpayd.ots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class OtsApplication {

    public static void main(String[] args) {
        SpringApplication.run(OtsApplication.class, args);
    }

}
