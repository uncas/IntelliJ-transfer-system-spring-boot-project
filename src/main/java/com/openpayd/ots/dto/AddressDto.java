package com.openpayd.ots.dto;

import lombok.Data;

@Data
public class AddressDto{

    private String info;
    private String city;
    private String country;

}