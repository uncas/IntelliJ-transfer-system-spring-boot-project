package com.openpayd.ots.util;

public final class AtsErrorMessages {

    public static final String TRANSACTION_UUID_EMPTY_ERROR_MESSAGE = "TransactionUUID can not bu null or empty";
    public static final String RATES_NOT_FOUND = "Rates Not Found";

}
