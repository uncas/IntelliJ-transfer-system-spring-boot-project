# IntelliJ-transfer-system-spring-boot-project
Transfer system project built with spring-boot and spring restful api.

This applicatin is a simple banking system. 

You can create an bank account and client,transfer money between two accounts, list all transfer transactions and get account and client detail.

All data stores in database and if an error occurs, the application interrupts the process and takes it back and informs with an error.

You can use and look services with swagger in http://localhost:8085/swagger-ui.html#/ when server is after started.

This application uses embeded Tomcat server, so you can just only run OtsApplication.java class and that's it.

This application uses PostgreSQL database and build with Maven structure.
